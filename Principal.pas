unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TService12 = class(TService)
    FDConn: TFDConnection;
    FDquery: TFDQuery;
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceAfterUninstall(Sender: TService);
    procedure ServiceBeforeInstall(Sender: TService);
    procedure ServiceBeforeUninstall(Sender: TService);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    procedure getDados;
    { Public declarations }
  end;

var
  Service12: TService12;
  arq: TextFile;
  wg_dados,nomeArq: String;

implementation

uses LogUtils;

{$R *.dfm}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  Service12.Controller(CtrlCode);
end;

procedure TService12.getDados;
begin
  NomeArq:= 'dados.log';

//---------------------Consulta-----------------------
  FDquery.SQL.Clear;
  FDquery.SQL.Add('select Nome from Clientes where ID = 1');
  FDquery.Open;
  wg_dados := FDquery.Fields[0].value;
  FDquery.close;
//----------------------------------------------------

  AssignFile(arq, nomeArq);
  if FileExists(nomeArq) then
    Append (arq)
  else
    ReWrite(arq);

  WriteLn(arq, wg_dados+' - ' + DateTimeToStr(Now));
  CloseFile(arq);

end;

function TService12.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TService12.ServiceAfterInstall(Sender: TService);
begin
  Log('Servi�o-Instalado-com-sucesso');
  getDados;
end;

procedure TService12.ServiceAfterUninstall(Sender: TService);
begin
  Log('Servi�o-Desinstalado-com-sucesso');
end;

procedure TService12.ServiceBeforeInstall(Sender: TService);
begin
  Log('Servi�o-Instalando');
end;

procedure TService12.ServiceBeforeUninstall(Sender: TService);
begin
  Log('Servi�o-Desinstalando');
end;

procedure TService12.ServiceContinue(Sender: TService; var Continued: Boolean);
begin
  Log('Servi�o-Continuando');
  Continued := true;
end;

procedure TService12.ServiceExecute(Sender: TService);
begin
  Log('Servi�o-em-execu��o');

   while not Self.Terminated do
   begin
     Sleep(200);
     ServiceThread.ProcessRequests(True);
   end;
end;

procedure TService12.ServicePause(Sender: TService; var Paused: Boolean);
begin
  Log('Servi�o-Pausado');
  Paused := true;
end;

procedure TService12.ServiceShutdown(Sender: TService);
begin
  Log('Servi�o-Desligado');
end;

procedure TService12.ServiceStart(Sender: TService; var Started: Boolean);
begin
  Log('Servi�o-Iniciado');
  Started := true;
end;

procedure TService12.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  Log('Servi�o-Parado');
  Stopped := true;
end;

end.
